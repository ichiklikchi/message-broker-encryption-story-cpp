#include "Handler.h"
#include <openssl/ssl.h>

namespace mb {
    
    Handler::Handler(struct ev_loop* loop)
        : AMQP::LibEvHandler(loop) {

        }

    // after the TLS connection has been created
    bool Handler::onSecured(AMQP::TcpConnection* connection, const SSL* ssl) {
        
        // https://docs.huihoo.com/doxygen/openssl/1.0.1c/structssl__st.html
        // implement your own CA checking
        //X509* cert = SSL_get_peer_certificate(ssl);
        /// ... 
        return SSL_get_verify_result(ssl) == X509_V_OK;
    }
    
    // before the TLS connection has been created
    bool Handler::onSecuring(AMQP::TcpConnection* connection, SSL* ssl) {
        return SSL_get_verify_result(ssl) == X509_V_OK;
    }
    
    void Handler::onProperties(AMQP::TcpConnection* connection, const AMQP::Table& server, AMQP::Table& client) {
        // we can check name of server or some specific field that only send from our server
        if(not server.contains("server-name")) {
            connection->close();
        }
    }

}  // namesapce bm
