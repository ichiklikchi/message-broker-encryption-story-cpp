#include <iostream>
#include "Handler.h"
#include <openssl/ssl.h>

int main(void) {

    OPENSSL_init_ssl(0, NULL);

    //access to the event loop
    auto* loop = EV_DEFAULT;
    mb::Handler handler(loop);

    // create a AMQP connection object
    AMQP::TcpConnection connection(&handler, "amqps://guest:guest@localhost/");

    // and create a channel
    AMQP::TcpChannel channel(&connection);

    // use the channel object to call the AMQP method you like
    channel.declareExchange("consumer", AMQP::fanout);
    channel.declareQueue("producer-consumer");

    auto callback = [](const AMQP::Message &message, uint64_t deliveryTag, bool redelivered) {
        std::cout << " [x] Received " << message.body() << std::endl;
    };

    channel.consume("producer-consumer", AMQP::noack).onReceived(callback);

    std::cout << " [*] Waiting for messages. To exit press CTRL-C\n";

    ev_run(loop, 0);
    return 0;
}