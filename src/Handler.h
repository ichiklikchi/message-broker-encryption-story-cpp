#pragma once

#include <memory>
#include <sstream>
#include <amqpcpp.h>
#include <amqpcpp/libev.h>

namespace mb {

    class Handler : public AMQP::LibEvHandler {
        public:
            // Constructors and destructor
            Handler(struct ev_loop* loop);
            ~Handler() = default;

            Handler(const Handler&) = delete;
            Handler& operator=(const Handler&) = delete;

        private:
            // AMQP::TcpHandler interface
            bool onSecured(AMQP::TcpConnection* connection, const SSL* ssl) override;
            bool onSecuring(AMQP::TcpConnection* connection, SSL* ssl) override;
            void onProperties(AMQP::TcpConnection* connection, const AMQP::Table& server, AMQP::Table& client) override;
    };

}