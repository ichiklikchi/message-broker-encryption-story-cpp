#include <iostream>
#include "Handler.h"
#include <openssl/ssl.h>

int main(void) {

    OPENSSL_init_ssl(0, NULL);

    //access to the event loop
    auto* loop = EV_DEFAULT;
    mb::Handler handler(loop);

    // create a AMQP connection object
    AMQP::TcpConnection connection(&handler, "amqps://guest:guest@localhost/");
    
     // and create a channelm
    AMQP::TcpChannel channel(&connection);
    
    const std::string message = "Hello World!";

    auto callback = [&]{
        // send message
        channel.publish("consumer", "producer-consumer", message.c_str());
        std::cout << " [x] Sent " << message << std::endl;
    };

    channel.onReady(callback);

    ev_run(loop, 0);
    return 0;
}