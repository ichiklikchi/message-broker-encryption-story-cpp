# LDAP Injection Story

**To read**: [https://cheatsheetseries.owasp.org]

**Estimated reading time**: 15 min

## Story Outline

This story is focused on providing guidance for preventing message broker encryption
in your applications.

Encrypted messaging apps get around this problem by encrypting their messages end-to-end. That means your app encrypts your messages before sending them to the server, and the recipient decrypts them on their end locally. Even the company's operators can't access any of your communications, as long as the encryption keys remain uncompromised.

Encrypted messaging means that hackers couldn't potentially intercept your communications and extract details like payment information or personal data that can then be used to access accounts

## Story Organization

**Story Branch**: master
>`git checkout master`

**Practical task tag for self-study**: task
>`git checkout task`

Tags: #injection, #security, #broker, #message
